import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom'

class Header extends Component {


    render() {

        const isActive = (match, location) => {
            console.log(match)
            return false;
        }

        return (
            <nav className="navbar navbar-inverse navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">

                        <Link className="navbar-brand" to="/">Project name</Link>
                    </div>
                    <div id="navbar" className="collapse navbar-collapse">
                        <ul className="nav navbar-nav">
                            <li><NavLink isActive={(match, location) => {
                                console.log(location);
                                return location.pathname == '/';
                            }} to="/">Home</NavLink></li>
                            <li><NavLink activeStyle={{color: 'red'}} to="/about">About</NavLink></li>
                            <li><NavLink to="/contact">Contact</NavLink></li>
                        </ul>
                    </div>

                </div>
            </nav>
        );
    }
}

export default Header;
