import React, {Component} from 'react'
import {Menu} from 'semantic-ui-react'
import {Link} from 'react-router-dom'

export default class MenuBar extends Component {
    state = {}

    handleItemClick = (e, {name}) => this.setState({activeItem: name})

    render() {
        const {activeItem} = this.state

        return (
            <Menu   stackable={true}>
                <Menu.Item
                    link={true}
                    name='editorials'
                    active={activeItem === 'editorials'}
                    onClick={this.handleItemClick}
                    as={Link}
                    to='/'
                >
                    خانه
                </Menu.Item>

                <Menu.Item name='about' active={activeItem === 'about'} onClick={this.handleItemClick}
                           as={Link}
                           to='/about'>
                    Reviews
                </Menu.Item>

                <Menu.Item
                    name='upcomingEvents'
                    active={activeItem === 'upcomingEvents'}
                    onClick={this.handleItemClick}
                >
                    Upcoming Events
                </Menu.Item>
            </Menu>
        )
    }
}