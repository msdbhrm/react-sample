import React, {Component} from 'react';
import './App.css';

import {Route} from "react-router-dom";
import Home from "./component/HomepageLayout";
import About from "./component/About";
import Contact from "./component/Contact";



class App extends Component {
    render() {


        return (
            <div>
                {/*<MenuBar/>*/}


                <Route exact path="/" component={Home}/>
                <Route path="/about/:username" component={About}/>
                <Route path="/contact" component={Contact}/>


            </div>


        );
    }
}

export default App;
